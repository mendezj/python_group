#include <iostream>
using namespace std;

#define DIM 3

class game_state {
public:
    int *data;
    game_state() { data = new int[9]; }
    ~game_state(){}
};

int win_vert (game_state *state, int i, int sym)
{
    int *a = state->data;

    if (a[i]==sym && a[i+3]==sym && a[i+6]==sym) {
        return sym;
    }
    return -1;
}

int win_horiz (game_state *state, int i, int sym)
{
    int *a = state->data;

    if (a[i]==sym && a[i+1]==sym && a[i+2]==sym) {
        return sym;
    }
    return -1;
}

int win_diag (game_state *state, int i, int sym)
{
    int *a = state->data;

    // first diagonal check top to bottom left to right
    if(a[i]==sym && a[i+DIM+1]==sym && a[i+DIM*2+2]==sym) {
        return sym;
    }
    // second diagonal check top to bottom right to left
    if(a[i]==sym && a[i+DIM-1]==sym && a[i+DIM*2-2]==sym) {
        return sym;
    }
    return -1;
}

void display_board (game_state *state)
{
    int count = 0;
    for (int i=0; i<DIM*DIM; i++) {
        cout << state->data[i] << " ";
        if (count == DIM-1) {
            cout << endl;
            count = 0;
            continue;
        }
        count++; 
    }
    cout << endl;
}

int solve( game_state *state )
{
}

int main()
{
    game_state *state = new game_state();

    for (int i=0; i<DIM*DIM; i++) {
        cin >> state->data[i];
    }

    //int ret = win_vert( state, 0, 1 ); 
    //int ret = win_horiz( state, 6, 0 );
    int ret = win_diag( state, 2, 0 );
    cout << "ret: " << ret << endl;

    //display_board(state);
}
